import java.util.ArrayList;
import java.util.List;

public class Wrapper<T> {
    List<T> list = new ArrayList<T>();
    int count = 0;

    public int size(){
        return list.size();
    }

    public void addItem(T value) throws DuplicatedException {

        for (int i = 0; i < list.size(); i++) {
            if (value == getItem(i)) {
                count++;
            }
        }

        if (value == null) {
            throw new NullPointerException("Inputted null value");
        } else if (count != 0) {
            throw new DuplicatedException("Duplicate Value : " + value);
        } else {
            list.add(value);
        }
    }

    public T getItem(int index) {
        return list.get(index);
    }
}
