public class DuplicatedException extends Exception {
    public DuplicatedException(String errorMessage){
        super(errorMessage);
    }
}
