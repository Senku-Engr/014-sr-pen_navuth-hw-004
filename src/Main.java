public class Main {

    public static void main(String[] args) {
        Wrapper<String> integerWrapper = new Wrapper<String>();

        try{

            integerWrapper.addItem("Hina");
            integerWrapper.addItem("Hodaka");
            integerWrapper.addItem("Hina");     // duplicate

        }catch (Exception e){
            System.out.println(e);
        }

        System.out.println("\nList all from inputted");
        for (int i=0; i<integerWrapper.size();i++){
            System.out.println(integerWrapper.getItem(i));
        }
    }
}
